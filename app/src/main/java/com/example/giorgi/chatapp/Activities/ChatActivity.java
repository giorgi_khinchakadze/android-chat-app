package com.example.giorgi.chatapp.Activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.giorgi.chatapp.Adapters.ChatRecyclerAdapter;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.Objects.Message;
import com.example.giorgi.chatapp.Events.Interfaces.MessageEventListener;
import com.example.giorgi.chatapp.Events.MessageEvents;
import com.example.giorgi.chatapp.R;
import com.example.giorgi.chatapp.SocketIO.SocketIO;
import com.example.giorgi.chatapp.Utilities;
import com.example.giorgi.chatapp.databinding.ChatActivityBinding;

import java.util.List;
import java.util.Objects;

import io.socket.client.Ack;

/**
 * Created by giorgi on 01.12.2016 21:59.
 */
public class ChatActivity extends Activity {

    private Contact contact;
    private TextView messageBox;
    private MessageEventListener listener;
    private ChatRecyclerAdapter adapter;
    private boolean isConsuming;


    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contact = getIntent().getParcelableExtra("contact");
        ChatActivityBinding binding  = DataBindingUtil.setContentView(this,R.layout.chat_activity);
        binding.setContact(contact);


        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ((LinearLayoutManager)recyclerView.getLayoutManager()).setStackFromEnd(true);


        adapter = new ChatRecyclerAdapter(contact, (LinearLayoutManager) recyclerView.getLayoutManager(),getApplicationContext());
        recyclerView.setAdapter(adapter);


        listener = new MessageEventListener() {
            @Override
            public boolean onMessageReceived(Message message) {
                if (Objects.equals(message.getSender().getUsername(), contact.getUsername())){
                    adapter.addToBottom(message);
                    return isConsuming;
                }
                return false;
            }

            @Override
            public void onMessagesConsumed(String username) {
            }

            @Override
            public void onMessageSent(Message message) {
                adapter.addToBottom(message);
            }

        };
        MessageEvents.registerListener(listener);

        messageBox = (TextView) findViewById(R.id.messageBox);

        AppCompatButton b = (AppCompatButton) findViewById(R.id.send);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (messageBox.getText().length() > 0){
                    MessageEvents.SendMessage(contact,messageBox.getText().toString());
                    messageBox.setText("");
                }
            }
        });

        findViewById(R.id.contact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.getContext().startActivity(new Intent(view.getContext(),ContactInfoActivity.class).putExtra("contact",contact));
            }
        });





        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (adapter.isLoading()){
                    swipeRefreshLayout.setRefreshing(false);
                    return;
                }

                adapter.setLoading(true);
                SocketIO.GetMessagesBefore(adapter.getFirstMessageId(),contact.getUsername(), new Ack() {
                    @Override
                    public void call(Object... args) {
                        final List<Message> messages =Utilities.JsonToMessageList((String) args[0]);
                        new Handler(getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                for (Message message : messages) {
                                    adapter.addToTop(message);
                                }
                                adapter.setLoading(false);
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        });
                    }
                });
            }
        });
    }



    @Override
    protected void onDestroy() {
        MessageEvents.unregisterListener(listener);
        adapter.destroy();
        super.onDestroy();
    }


    @Override
    protected void onResume() {
        isConsuming = true;
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.cancel(contact.getUsername().hashCode());
        MessageEvents.MessageConsumed(contact.getUsername());

        SocketIO.ConsumeMessages(contact,null);

        super.onResume();
    }

    @Override
    protected void onPause() {
        isConsuming = false;
        super.onPause();
    }
}
