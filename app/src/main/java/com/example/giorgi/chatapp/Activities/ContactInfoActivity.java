package com.example.giorgi.chatapp.Activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.R;
import com.example.giorgi.chatapp.SocketIO.SocketIO;
import com.example.giorgi.chatapp.databinding.ActivityContactInfoBinding;

import io.socket.client.Ack;

public class ContactInfoActivity extends AppCompatActivity {

    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityContactInfoBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_contact_info);

        contact = getIntent().getParcelableExtra("contact");
        boolean inContacts = getIntent().getBooleanExtra("in_contacts", false);

        binding.setContact(contact);

        Button btn = (Button) findViewById(R.id.add_btn);
        btn.setVisibility(!inContacts ? View.VISIBLE:View.GONE);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SocketIO.AddContact(contact, new Ack() {
                    @Override
                    public void call(Object... args) {
                        finish();
                    }
                });
            }
        });

    }




}
