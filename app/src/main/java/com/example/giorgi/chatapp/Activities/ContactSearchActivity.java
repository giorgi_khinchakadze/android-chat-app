package com.example.giorgi.chatapp.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ProgressBar;

import com.example.giorgi.chatapp.Adapters.ContactSearchRecyclerAdapter;
import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.R;
import com.example.giorgi.chatapp.SocketIO.SocketIO;
import com.example.giorgi.chatapp.Utilities;

import java.util.List;

import io.socket.client.Ack;

public class ContactSearchActivity extends AppCompatActivity {


    private ProgressBar spinner;
    private RecyclerView list;
    private TextInputEditText username;
    private ContactSearchRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_search);

        spinner = (ProgressBar) findViewById(R.id.spinner);
        list = (RecyclerView) findViewById(R.id.list);
        username = (TextInputEditText) findViewById(R.id.username);

        list.setLayoutManager(new LinearLayoutManager(this));

        adapter = new ContactSearchRecyclerAdapter();
        list.setAdapter(adapter);

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                refreshList();
            }


        });

    }

    private void refreshList(){
        if (username.getText().toString().isEmpty()) return;

        spinner.setVisibility(View.VISIBLE);
        list.setVisibility(View.GONE);
        SocketIO.GetContactListFor(username.getText().toString(), new Ack() {
            @Override
            public void call(Object... args) {
                if (args[0].equals(Constants.SEARCH_SUCCESS)){
                    final List<Contact> contacts = Utilities.JsonToContactList(args[1].toString());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.addContacts(contacts);
                            spinner.setVisibility(View.GONE);
                            list.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshList();
    }

}
