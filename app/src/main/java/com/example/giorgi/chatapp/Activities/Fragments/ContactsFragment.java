package com.example.giorgi.chatapp.Activities.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.giorgi.chatapp.Activities.ContactSearchActivity;
import com.example.giorgi.chatapp.Adapters.ContactsRecyclerAdapter;
import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Events.ContactEvents;
import com.example.giorgi.chatapp.Events.Interfaces.ContactEventListener;
import com.example.giorgi.chatapp.R;
import com.example.giorgi.chatapp.SocketIO.SocketIO;
import com.example.giorgi.chatapp.SocketIO.SocketStatusChangeListener;

/**
 * Created by giorgi on 30.11.2016 19:12.
 */
public class ContactsFragment extends Fragment {
    private RecyclerView recyclerView = null;
    private ContactsRecyclerAdapter adapter;

    private ContactEventListener listener;
    private SocketStatusChangeListener socketStatusChangeListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.contacts_fragment,container,false);


        recyclerView = (RecyclerView) v.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        recyclerView.setAdapter(new ContactsRecyclerAdapter(v.getContext().getApplicationContext()));
        adapter = (ContactsRecyclerAdapter) recyclerView.getAdapter();

        AppCompatEditText text = (AppCompatEditText) v.findViewById(R.id.search);
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                ((ContactsRecyclerAdapter)recyclerView.getAdapter()).getFilterList(editable.toString());
            }
        });



        listener = new ContactEventListener() {
            @Override
            public void contactDeleted(String username) {
                adapter.deleteContact(username);
            }

            @Override
            public void contactAdded(Contact contact) {
                adapter.addContact(contact);
            }

            @Override
            public void contactStatusChanged(String username, int status) {
                adapter.setContactStatus(username,status);
            }
        };

        socketStatusChangeListener = new SocketStatusChangeListener() {
            @Override
            public void statusChanged(boolean status) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.setContactStatus(null, Constants.CONTACT_STATUS_OFFLINE);
                    }
                });
            }
        };

        ContactEvents.RegisterListener(listener);
        SocketIO.registerStatusChangeListener(socketStatusChangeListener);

        v.findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ContactSearchActivity.class));
            }
        });

        return v;
    }

    @Override
    public void onDestroyView() {
        adapter.destroy();
        ContactEvents.UnregisterListener(listener);
        SocketIO.unregisterStatusChangeListener(socketStatusChangeListener);
        super.onDestroyView();
    }
}
