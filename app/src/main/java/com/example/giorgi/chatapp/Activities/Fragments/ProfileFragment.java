package com.example.giorgi.chatapp.Activities.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.giorgi.chatapp.Activities.LoginActivity;
import com.example.giorgi.chatapp.Events.ContactEvents;
import com.example.giorgi.chatapp.Events.MessageEvents;
import com.example.giorgi.chatapp.R;
import com.example.giorgi.chatapp.Self;
import com.example.giorgi.chatapp.SocketIO.SocketIO;

/**
 * Created by giorgi on 30.11.2016 19:12.
 */
public class ProfileFragment extends Fragment {

    private SharedPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.profile_fragment,container,false);

        preferences = getActivity().getSharedPreferences("main",Context.MODE_PRIVATE);

        boolean sound = preferences.getBoolean("sound",true);
        boolean notification = preferences.getBoolean("notification",true);

        SwitchCompat s = (SwitchCompat) v.findViewById(R.id.sound);
        SwitchCompat n = (SwitchCompat) v.findViewById(R.id.notification);

        s.setChecked(sound);
        n.setChecked(notification);

        s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                preferences.edit().putBoolean("sound",b).apply();
            }
        });

        n.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                preferences.edit().putBoolean("notification",b).apply();
            }
        });


        ((TextView)v.findViewById(R.id.fullname)).setText(Self.Contact.getFirstname() + " " + Self.Contact.getLastname());
        ((TextView)v.findViewById(R.id.username)).setText(Self.Contact.getUsername());

        v.findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSharedPreferences("main",Context.MODE_PRIVATE).edit().clear().apply();

                SocketIO.Reset();
                MessageEvents.Reset();
                ContactEvents.Reset();

                startActivity(new Intent(getContext(), LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                getActivity().finish();
            }
        });

        return  v;

    }
}
