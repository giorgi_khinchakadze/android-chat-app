package com.example.giorgi.chatapp.Activities.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.giorgi.chatapp.Adapters.RecentMessagesAdapter;
import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.Objects.Message;
import com.example.giorgi.chatapp.Events.ContactEvents;
import com.example.giorgi.chatapp.Events.Interfaces.MessageEventListener;
import com.example.giorgi.chatapp.Events.Interfaces.ContactEventListener;
import com.example.giorgi.chatapp.Events.MessageEvents;
import com.example.giorgi.chatapp.R;
import com.example.giorgi.chatapp.SocketIO.SocketIO;
import com.example.giorgi.chatapp.SocketIO.SocketStatusChangeListener;

/**
 * Created by giorgi on 30.11.2016 19:12.
 */
public class RecentFragment extends Fragment {


    private RecentMessagesAdapter adapter;
    private MessageEventListener listener;

    private ContactEventListener contactEventListener;
    private SocketStatusChangeListener socketStatusChangeListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.recent_fragment,container,false);



        contactEventListener = new ContactEventListener() {
            @Override
            public void contactDeleted(String username) {
                adapter.removeEntry(username);
            }

            @Override
            public void contactAdded(Contact username) {

            }

            @Override
            public void contactStatusChanged(String username, int status) {
                adapter.toggleUserStatus(username,status);
            }
        };


        listener = new MessageEventListener() {
            @Override
            public boolean onMessageReceived(Message message) {
                adapter.newMessage(message);
                return false;
            }
            @Override
            public void onMessagesConsumed(String contact) {
                adapter.messageConsumed(contact);
            }

            @Override
            public void onMessageSent(Message message) {
                adapter.newMessage(message);
            }
        };


        socketStatusChangeListener = new SocketStatusChangeListener() {
            @Override
            public void statusChanged(boolean status) {
                if (status == Constants.SERVER_STATUS_UNRESPONSIVE){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.toggleUserStatus(null,Constants.CONTACT_STATUS_OFFLINE);
                        }
                    });
                }
            }
        };


        MessageEvents.registerListener(listener);
        ContactEvents.RegisterListener(contactEventListener);
        SocketIO.registerStatusChangeListener(socketStatusChangeListener);

        RecyclerView r = (RecyclerView) v.findViewById(R.id.recycler);
        r.setLayoutManager(new LinearLayoutManager(v.getContext()));
        r.setAdapter(new RecentMessagesAdapter(v.getContext().getApplicationContext(), (LinearLayoutManager) r.getLayoutManager()));
        adapter = (RecentMessagesAdapter) r.getAdapter();

        return v;
    }

    @Override
    public void onDestroyView() {
        adapter.destroy();
        MessageEvents.unregisterListener(listener);
        ContactEvents.UnregisterListener(contactEventListener);
        SocketIO.unregisterStatusChangeListener(socketStatusChangeListener);
        super.onDestroyView();
    }

}
