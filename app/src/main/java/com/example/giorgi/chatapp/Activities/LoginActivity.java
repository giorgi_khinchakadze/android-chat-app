package com.example.giorgi.chatapp.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.R;
import com.example.giorgi.chatapp.Self;
import com.example.giorgi.chatapp.SocketIO.SocketIO;

import io.socket.client.Ack;

public class LoginActivity extends Activity{

    private static final int REGISTER_ACTIVITY = 1;

    // UI references.
    private TextInputEditText usernameView;
    private TextInputEditText passwordView;
    private TextView error;
    private boolean waitingForResult = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameView = (TextInputEditText) findViewById(R.id.email);
        passwordView = (TextInputEditText) findViewById(R.id.password);
        error = (TextView) findViewById(R.id.error);

        SocketIO.getStatus();

        final String token = Self.getOldToken(this);
        if (!token.isEmpty()){
            waitingForResult = true;
            loginSuccess();
        }


        Button loginButton = (Button) findViewById(R.id.email_sign_in_button);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (waitingForResult) return;
                attemptLogin();
            }
        });

        findViewById(R.id.register).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (waitingForResult) return;
                startActivityForResult(new Intent(getApplicationContext(),RegisterActivity.class),REGISTER_ACTIVITY);
            }
        });


    }




    private void attemptLogin() {

        if (SocketIO.getStatus() == Constants.SERVER_STATUS_UNRESPONSIVE){
            error.setText(R.string.server_down_text);
            return;
        }

        error.setText(null);

        String username = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        waitingForResult = true;
        SocketIO.LoginUser(username, password, new Ack() {
            @Override
            public void call(final Object... args) {
                waitingForResult = false;
                new Handler(getApplicationContext().getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (args[0].toString().equals(Constants.LOGIN_SUCCESS)){
                            Self.setNewInfo(getApplicationContext(),args[1].toString());
                            loginSuccess();
                        }else {
                            error.setText(args[1].toString());
                        }

                    }
                });

            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REGISTER_ACTIVITY){
            if (resultCode == RESULT_OK){
                loginSuccess();
            }
        }
    }


    private void loginSuccess(){
        startActivity(new Intent(this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }


}

