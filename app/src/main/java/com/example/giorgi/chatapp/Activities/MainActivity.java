package com.example.giorgi.chatapp.Activities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;

import com.example.giorgi.chatapp.Activities.Fragments.ContactsFragment;
import com.example.giorgi.chatapp.Activities.Fragments.ProfileFragment;
import com.example.giorgi.chatapp.Activities.Fragments.RecentFragment;
import com.example.giorgi.chatapp.AsyncTasks.MessageMarker;
import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.Database.DatabaseFunctions;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.Objects.Message;
import com.example.giorgi.chatapp.Events.ContactEvents;
import com.example.giorgi.chatapp.Events.Interfaces.ContactEventListener;
import com.example.giorgi.chatapp.Events.Interfaces.MessageEventListener;
import com.example.giorgi.chatapp.Events.MessageEvents;
import com.example.giorgi.chatapp.R;
import com.example.giorgi.chatapp.Self;
import com.example.giorgi.chatapp.SocketIO.SocketIO;
import com.example.giorgi.chatapp.SocketIO.SocketStatusChangeListener;
import com.example.giorgi.chatapp.Utilities;
import com.facebook.stetho.Stetho;

import java.util.List;

import io.socket.client.Ack;


//TODO: fix sometimes sending request but not getting relies

public class MainActivity extends AppCompatActivity {


    private SocketStatusChangeListener listener;
    private MessageEventListener messageEventListener;
    private ContactEventListener contactEventListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.initDebug();


        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_home_black_48dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_contacts_black_48dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_account_circle_black_48dp);


        listener = new SocketStatusChangeListener() {
            @Override
            public void statusChanged(boolean status) {
                if (status == Constants.SERVER_STATUS_CONNECTED) {
                    tryTokenLogin();
                }
            }
        };


        messageEventListener = new MessageEventListener() {
            @Override
            public boolean onMessageReceived(Message message) {
                DatabaseFunctions.saveMessage(message,getApplicationContext());
                if (message.getStatus() == Constants.MESSAGE_STATUS_NOT_READ)
                    fireNotificationIfWanted(message,getApplicationContext());
                return false;
            }

            @Override
            public void onMessagesConsumed(String username) {
                ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).cancel(username.hashCode());
                new MessageMarker(username, getApplicationContext()).execute();
            }

            @Override
            public void onMessageSent(Message message) {
                DatabaseFunctions.saveMessage(message,getApplicationContext());
            }

        };
        contactEventListener = new ContactEventListener() {
            @Override
            public void contactDeleted(final String username) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        DatabaseFunctions.removeContact(username,getApplicationContext());
                    }
                }).start();
            }

            @Override
            public void contactAdded(Contact contact) {
                DatabaseFunctions.addContact(contact,getApplicationContext());
            }

            @Override
            public void contactStatusChanged(String username, int status) {
                DatabaseFunctions.updateContactStatus(username,status,getApplicationContext());
            }
        };


        MessageEvents.registerListener(messageEventListener);
        ContactEvents.RegisterListener(contactEventListener);
        SocketIO.registerStatusChangeListener(listener);

        tryTokenLogin();
    }


    private void tryTokenLogin(){

        SocketIO.TokenLogin(Self.Token, new Ack() {
            @Override
            public void call(final Object... args) {
                new Handler(getApplicationContext().getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (args[0].toString().equals(Constants.TOKEN_LOGIN_SUCCESS)) {
                            Self.setNewInfo(getApplicationContext(), args[1].toString());
                            updateLocalData();
                            return;
                        }
                        if (args[0].toString().equals(Constants.TOKEN_LOGIN_FAILURE)) {
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    }
                });
            }
        });

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0: return new RecentFragment();
                case 1: return new ContactsFragment();
                case 2: return new ProfileFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        SocketIO.unregisterStatusChangeListener(listener);
        MessageEvents.unregisterListener(messageEventListener);
        ContactEvents.UnregisterListener(contactEventListener);

        DatabaseFunctions.updateContactStatus(null,Constants.CONTACT_STATUS_OFFLINE,this);

    }


    private void updateLocalData(){

        SocketIO.GetCurrentContactList(new Ack() {
            @Override
            public void call(Object... args) {
                if (args[0].toString().equals(Constants.CONTACT_LIST_SUCCESS)){
                    final List<Contact> list = Utilities.JsonToContactList(args[1].toString());

                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            for (Contact contact : list) {
                                ContactEvents.ContactStatusChanged(contact.getUsername(),contact.getStatus());
                            }
                            DatabaseFunctions.updateContactsList( list,getApplicationContext());
                        }
                    });
                }

            }
        });


        new Thread(new Runnable() {
            @Override
            public void run() {
                int last = DatabaseFunctions.GetLatestMessageId(getApplicationContext());
                SocketIO.RequestMessagesAfter(last);
            }
        }).start();


    }


    private static void fireNotificationIfWanted(Message m , Context context){


        if ( !context.getSharedPreferences("main",Context.MODE_PRIVATE).getBoolean("notification",true)) return;

        Contact contact = m.getSender();


        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_sms_black_24dp)
                        .setContentTitle(contact.getUsername())
                        .setContentText(m.getText());


        Intent intent = new Intent(context, ChatActivity.class);

        intent.putExtra("contact",contact);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(ChatActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);


        if (context.getSharedPreferences("main",Context.MODE_PRIVATE).getBoolean("sound",true)){
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder.setSound(soundUri);
        }


        Notification notification = builder.build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(contact.getUsername().hashCode(),notification);

    }



    private void initDebug() {
        Stetho.initializeWithDefaults(this);
    }




}
