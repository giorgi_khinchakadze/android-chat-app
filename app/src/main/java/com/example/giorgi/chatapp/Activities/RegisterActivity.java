package com.example.giorgi.chatapp.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.R;
import com.example.giorgi.chatapp.Self;
import com.example.giorgi.chatapp.SocketIO.SocketIO;

import io.socket.client.Ack;

public class RegisterActivity extends AppCompatActivity {


    private TextInputEditText username;
    private TextInputEditText password;
    private TextInputEditText passwordRepeat;
    private TextInputEditText firstname;
    private TextInputEditText lastname;
    private TextView error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username = (TextInputEditText) findViewById(R.id.username);
        password = (TextInputEditText) findViewById(R.id.password);
        passwordRepeat = (TextInputEditText) findViewById(R.id.password_repeat);
        firstname = (TextInputEditText) findViewById(R.id.firstname);
        lastname = (TextInputEditText) findViewById(R.id.lastname);
        error = (TextView) findViewById(R.id.error);


        findViewById(R.id.register_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });

    }

    private void register() {
        clearErrors();

        if (SocketIO.getStatus() == Constants.SERVER_STATUS_UNRESPONSIVE){
            error.setText(R.string.server_down_text);
            return;
        }

        if (!password.getText().toString().equals(passwordRepeat.getText().toString())){
            passwordRepeat.setError(getString(R.string.password_mismatch));
            return;
        }

        SocketIO.RegisterUser(username.getText().toString(), firstname.getText().toString(), lastname.getText().toString(), password.getText().toString(), new Ack() {
            @Override
            public void call(final Object... args) {
                new Handler(getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (args[0].toString().equals(Constants.REGISTER_SUCCESS)){
                            setResult(RESULT_OK);

                            Self.setNewInfo(getApplicationContext(),(String) args[1]);

                            finish();
                        }
                        else {
                            setErrors(args[1].toString());
                        }
                    }
                });

            }
        });
    }


    private void clearErrors(){
        error.setText(null);
        passwordRepeat.setError(null);
        password.setError(null);
        firstname.setError(null);
        lastname.setError(null);
        username.setError(null);
    }

    // TODO: implement properly
    private void setErrors(String errors){
        error.setText(errors);
    }

}
