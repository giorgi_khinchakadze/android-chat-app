package com.example.giorgi.chatapp.Adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.giorgi.chatapp.AsyncTasks.MessagesLoader;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.Objects.Message;
import com.example.giorgi.chatapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by giorgi on 03.12.2016 16:24.
 */
public class ChatRecyclerAdapter extends RecyclerView.Adapter {
    private final int SENT = 1;
    private final int RECEIVED = 2;

    private final Contact contact;

    private AsyncTask task;
    private final List<MessageGroup> messageGroups = new ArrayList<>();
    private final LinearLayoutManager manager;
    private final Context context;

    private boolean isLoading = true;


    public ChatRecyclerAdapter(Contact contact, LinearLayoutManager manager,Context context) {
        this.contact = contact;
        this.manager = manager;
        this.context = context;
        this.loadMessages();
    }

    public boolean isLoading(){
        return isLoading;
    }
    public void setLoading(boolean a){
        isLoading = a;
    }

    private void loadMessages() {
        task = new MessagesLoader(this,contact,context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public int getFirstMessageId(){
        try {
            return messageGroups.get(0).getMessages().get(0).getId();
        }catch (Exception e){
            return -1;
        }
    }


    //TODO: canceled message grouping ( it looked horrible )
    public void addToBottom(Message m){
        MessageGroup last = null;
        if (messageGroups.size() > 0) last = messageGroups.get(messageGroups.size() - 1);

        if (last != null && Objects.equals(last.getSender(), m.getSender().getUsername()) && false){
            last.addMessage(m);
            this.notifyItemChanged(messageGroups.size() - 1);
        }else {
            MessageGroup g = new MessageGroup();
            g.addMessage(m);
            messageGroups.add(g);
            this.notifyItemInserted(messageGroups.size() - 1);
        }
        manager.scrollToPosition(messageGroups.size() - 1);

    }

    public void addToTop(Message m){
        MessageGroup first = null;
        if (messageGroups.size() > 0) first = messageGroups.get(0);

        if (first != null && Objects.equals(first.getSender(), m.getSender().getUsername()) && false){
            first.messages.add(0,m);
            this.notifyItemChanged(0);
        }else {
            MessageGroup g = new MessageGroup();
            g.addMessage(m);
            messageGroups.add(0,g);
            this.notifyItemInserted(0);
        }

    }


    public void destroy(){
        if (task != null)
            task.cancel(true);
    }

    private static class MessageGroup{
        final List<Message> messages = new LinkedList<>();

        void addMessage(Message m){
            messages.add(m);
        }
        String getSender(){
            if (messages.size() == 0) return  "";
            return messages.get(0).getSender().getUsername();
        }
        List<Message> getMessages(){return messages;}
    }

    @Override
    public int getItemViewType(int position) {
        return (Objects.equals(messageGroups.get(position).getSender(), contact.getUsername()))?RECEIVED: SENT;
    }

    @Override
    public int getItemCount() {
        return messageGroups.size();
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AbstractViewHolder h = (AbstractViewHolder) holder;
        h.fill(messageGroups.get(position));
    }


    abstract static class AbstractViewHolder extends RecyclerView.ViewHolder{
        AbstractViewHolder(View v){
            super(v);
        }
        abstract void fill(MessageGroup g);
    }

    private AbstractViewHolder inflateReceived(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.received_message,parent,false);
        return new ReceivedMessagesViewHolder(v,contact);
    }

    private AbstractViewHolder inflateSelf(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sent_message,parent,false);
        return new SentMessagesViewHolder(v);
    }

    @Override
    public AbstractViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType){
            case SENT:
                return inflateSelf(parent);
            case RECEIVED:
                return inflateReceived(parent);
        }

        return null;
    }

    private static class SentMessagesViewHolder extends AbstractViewHolder {
        final LinearLayout layout;
        SentMessagesViewHolder(View v) {
            super(v);
            layout = (LinearLayout) v.findViewById(R.id.messages);
        }

        @Override
        void fill(MessageGroup g) {
            layout.removeAllViews();
            for (Message message : g.getMessages()) {
                TextView t = (TextView) LayoutInflater.from(layout.getContext()).inflate(R.layout.message,layout,false);
                t.setText(message.getText());
                layout.addView(t);
            }
        }
    }

    private static class ReceivedMessagesViewHolder extends AbstractViewHolder {
        final LinearLayout layout;
        final ImageView image;
        final Contact sender;
        ReceivedMessagesViewHolder(View v,Contact sender) {
            super(v);
            layout = (LinearLayout) v.findViewById(R.id.messages);
            image = (ImageView) v.findViewById(R.id.sender_photo);
            this.sender = sender;
        }

        @Override
        void fill(MessageGroup g) {

            Picasso.with(image.getContext()).load(sender.getImg()).fit().placeholder(R.drawable.ic_account_circle_black_48dp).into(image);

            layout.removeAllViews();
            for (Message message : g.getMessages()) {
                TextView t = (TextView) LayoutInflater.from(layout.getContext()).inflate(R.layout.message,layout,false);
                t.setText(message.getText());
                layout.addView(t);
            }

        }
    }
}
