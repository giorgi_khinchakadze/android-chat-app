package com.example.giorgi.chatapp.Adapters;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;
import com.example.giorgi.chatapp.Activities.ContactInfoActivity;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.R;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by giorgi on 07.01.2017 14:27.
 */

public class ContactSearchRecyclerAdapter extends RecyclerView.Adapter {

    private List<Contact> contacts = new LinkedList<>();

    private class ContactViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding dataBinding;
        ContactViewHolder(ViewDataBinding v) {
            super(v.getRoot());
            this.dataBinding = v;
            dataBinding.executePendingBindings();
        }
        ViewDataBinding getDataBinding() {
            return dataBinding;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding v = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.contact,parent,false);
        return new ContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContactViewHolder h = (ContactViewHolder) holder;
        Contact c = contacts.get(position);
        h.getDataBinding().setVariable(BR.contact,c);

        h.itemView.setOnClickListener(new CustomOnClickListener(c));

    }


    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public void addContacts(List<Contact> contacts) {
        this.contacts = contacts;
        this.notifyDataSetChanged();
    }


    private class CustomOnClickListener implements View.OnClickListener {
        private final Contact c;
        CustomOnClickListener(Contact c) {
            this.c  = c;
        }

        @Override
        public void onClick(View view) {
            view.getContext().startActivity(new Intent(view.getContext(),ContactInfoActivity.class).putExtra("contact",c));
        }

    }
}
