package com.example.giorgi.chatapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;
import com.example.giorgi.chatapp.Activities.ChatActivity;
import com.example.giorgi.chatapp.AsyncTasks.DatabaseContactsLoader;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giorgi on 01.12.2016 19:25.
 */
public class ContactsRecyclerAdapter extends RecyclerView.Adapter {

    private final List<Contact> contacts = new ArrayList<>();
    private AsyncTask currentTask;
    private final Context context;

    public ContactsRecyclerAdapter(Context context) {
        this.context = context;
        this.getFilterList("");
    }

    public void addContact(Contact c){
        contacts.add(c);
        this.notifyItemInserted(contacts.size() - 1);
    }
    public void getFilterList(String filter){
        if (currentTask != null){
            currentTask.cancel(true);
        }
        contacts.clear();
        this.notifyDataSetChanged();
        currentTask = new DatabaseContactsLoader(context,filter,this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void setContactStatus(String username, int status){
        for (Contact contact : contacts) {
            if (username == null || contact.getUsername().equals(username)){
                contact.setStatus(status);
            }
        }
    }
    public void destroy(){
        if (currentTask != null){
            currentTask.cancel(true);
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding  v = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.contact,parent,false);

        return new ContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContactViewHolder h = (ContactViewHolder) holder;
        Contact c = contacts.get(position);

        h.getDataBinding().setVariable(BR.contact,c);

        holder.itemView.setOnClickListener(new CustomOnClickListener(c));
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public void deleteContact(String username) {
        for (int i = 0; i < contacts.size(); i++) {
            if (contacts.get(i).getUsername().equals(username)){
                contacts.remove(i);
                this.notifyItemRemoved(i);
                return;
            }
        }
    }

    private class ContactViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding dataBinding;
        ContactViewHolder(ViewDataBinding v) {
            super(v.getRoot());
            this.dataBinding = v;
            dataBinding.executePendingBindings();
        }
        ViewDataBinding getDataBinding() {
            return dataBinding;
        }
    }

    private class CustomOnClickListener implements View.OnClickListener {
        final Contact contact;
        CustomOnClickListener(Contact c) {
            contact = c;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(),ChatActivity.class);
            intent.putExtra("contact",contact);

            view.getContext().startActivity(intent);

        }
    }
}
