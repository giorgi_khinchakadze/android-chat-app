package com.example.giorgi.chatapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.giorgi.chatapp.Activities.ChatActivity;
import com.example.giorgi.chatapp.AsyncTasks.RecentConversationLoader;
import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.Objects.Message;
import com.example.giorgi.chatapp.R;
import com.example.giorgi.chatapp.Self;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by giorgi on 30.11.2016 19:23.
 */
public class RecentMessagesAdapter extends RecyclerView.Adapter {

    private final Context context;
    private final AsyncTask currentTask;
    private final LinearLayoutManager manager;

    private final List<Entry> entries = new LinkedList<>();

    public RecentMessagesAdapter(Context context, LinearLayoutManager manager){
        this.manager = manager;
        this.context = context;
        currentTask = new RecentConversationLoader(this,context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    public void destroy(){
        if(currentTask != null){
            currentTask.cancel(true);
        }
    }

    public void toggleUserStatus(String username,int status){
        for (int i = 0; i < entries.size(); i++) {
            if (username == null || Objects.equals(entries.get(i).contact.getUsername(), username)){
                entries.get(i).contact.setStatus(status);
                this.notifyItemChanged(i);
                break;
            }
        }
    }

    public void addEntry(Message message) {
        for (int i = 0; i < entries.size(); i++) {
            Message m = entries.get(i).message;
            if (m.getSent().before(message.getSent())){
                entries.add(i,new Entry(getNotSelf(message),message));
                this.notifyItemInserted(i);
                manager.scrollToPosition(0);
                return;
            }
        }

        entries.add(new Entry(getNotSelf(message),message));
        this.notifyItemInserted(entries.size() - 1);
        manager.scrollToPosition(0);

    }

    public void newMessage(Message m){
        for (int i = 0; i < entries.size(); i++) {
            Contact c = entries.get(i).contact;
            if (Objects.equals(c.getUsername(), m.getSender().getUsername()) || Objects.equals(c.getUsername(), m.getReceiver().getUsername())){
                Entry entry = entries.get(i);

                entry.message = m;
                entry.status = m.getStatus();
                entries.remove(i);
                entries.add(0,entry);
                this.notifyItemMoved(i,0);
                this.notifyItemChanged(0);
                manager.scrollToPosition(0);
                return;
            }
        }
        entries.add(0,new Entry(getNotSelf(m),m));
        this.notifyItemInserted(0);
        manager.scrollToPosition(0);
    }


    private Contact getNotSelf(Message message){
        if (!Objects.equals(message.getSender().getUsername(), Self.Contact.getUsername())) return message.getSender();
        return message.getReceiver();
    }


    public void messageConsumed(String username) {
        for (int i = 0; i < entries.size(); i++) {
            Contact c = entries.get(i).contact;
            if (Objects.equals(c.getUsername(), username)){
                entries.get(i).status = Constants.MESSAGE_STATUS_READ;
                this.notifyItemChanged(i);
                break;
            }
        }
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_message,parent,false);
        return new RecentMessageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RecentMessageViewHolder h = (RecentMessageViewHolder) holder;
        h.bind(entries.get(position),context);
        h.itemView.setOnClickListener(new CustomOnClickListener(entries.get(position).contact));
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }


    public void removeEntry(String username){
        for (int i = 0; i < entries.size(); i++) {
            Entry entry = entries.get(i);
            if (entry.contact.getUsername().equals(username)) {
                entries.remove(entry);
                this.notifyItemChanged(i);
            }
        }
    }

    private static class Entry {
        final Contact contact;
        Message message;
        int status;
        Entry(Contact c, Message m){
            this.contact = c;
            this.message = m;
            status = m.getStatus();
        }

        boolean isUnread() {
            return !message.getSender().getUsername().equals(Self.Contact.getUsername()) && status == Constants.MESSAGE_STATUS_NOT_READ;
        }


    }


    private static class RecentMessageViewHolder extends RecyclerView.ViewHolder {
        final ImageView image;
        final ImageView status;
        final TextView name;
        final TextView message;
        RecentMessageViewHolder(View v) {
            super(v);
            image = (ImageView) v.findViewById(R.id.image);
            status = (ImageView) v.findViewById(R.id.status);
            name = (TextView) v.findViewById(R.id.username);
            message = (TextView) v.findViewById(R.id.message);
        }

        void bind(Entry entry, Context context){
            Picasso.with(context).load(entry.contact.getImg()).fit().placeholder(R.drawable.ic_account_circle_black_48dp).into(image);
            Picasso.with(context).load(entry.contact.getStatus() == Constants.CONTACT_STATUS_ONLINE?R.drawable.new_moon:R.drawable.moon).fit().into(status);
            name.setText(entry.contact.getUsername());
            message.setText(entry.message.getText());
            if (entry.isUnread()){
                name.setTypeface(Typeface.DEFAULT_BOLD);
                message.setTypeface(Typeface.DEFAULT_BOLD);
            }else {
                name.setTypeface(Typeface.DEFAULT);
                message.setTypeface(Typeface.DEFAULT);
            }

        }

    }


    private static class CustomOnClickListener implements View.OnClickListener {
        final Contact contact;
        CustomOnClickListener(Contact c) {
            contact = c;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(),ChatActivity.class);
            intent.putExtra("contact",contact);

            view.getContext().startActivity(intent);

        }
    }
}
