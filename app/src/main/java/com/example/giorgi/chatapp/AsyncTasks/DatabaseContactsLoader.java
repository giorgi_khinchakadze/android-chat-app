package com.example.giorgi.chatapp.AsyncTasks;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import com.example.giorgi.chatapp.Adapters.ContactsRecyclerAdapter;
import com.example.giorgi.chatapp.Database.Database;
import com.example.giorgi.chatapp.Database.DatabaseFunctions;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.SQLiteClasses.DatabaseObjects;
import com.example.giorgi.chatapp.Self;


/**
 * Created by giorgi on 01.12.2016 20:08.
 */

public class DatabaseContactsLoader extends AsyncTask<Void,Contact,Void> {

    private final ContactsRecyclerAdapter adapter;
    private final String filter;
    private final Context context;

    public DatabaseContactsLoader(Context context, String filter, ContactsRecyclerAdapter adapter){
        this.context = context;
        this.adapter = adapter;
        this.filter = filter;
    }

    @Nullable
    @Override
    protected Void doInBackground(Void... strings) {
        Database.LOCK.lock();
        Database.LOCK.unlock();

        SQLiteDatabase db = Database.getDB(context);

        Cursor c = db.rawQuery("select * from " + DatabaseObjects.ContactsTable.TABLE_NAME +
                                        " where " + DatabaseObjects.ContactsTable.COLUMN_NAME + " like ? and " + DatabaseObjects.ContactsTable.COLUMN_OWNER + " = ?",
                                                        new String[]{"%" + filter + "%", Self.Contact.getUsername()});
        c.moveToFirst();
        while (!this.isCancelled() && !c.isAfterLast()){


            publishProgress(DatabaseFunctions.getContact(c));

            c.moveToNext();
        }
        c.close();

        return null;
    }



    @Override
    protected void onProgressUpdate(Contact... values) {
        if (!this.isCancelled())
            adapter.addContact(values[0]);
    }


}
