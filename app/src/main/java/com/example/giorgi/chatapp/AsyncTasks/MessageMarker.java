package com.example.giorgi.chatapp.AsyncTasks;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.Database.Database;
import com.example.giorgi.chatapp.Database.SQLiteClasses.DatabaseObjects;
import com.example.giorgi.chatapp.Self;

/**
 * Created by giorgi on 08.01.2017 18:58.
 */

public class MessageMarker extends AsyncTask<Void,Void,Void> {

    private final String username;
    private final Context context;

    public MessageMarker(String username, Context context){
        this.username = username;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        SQLiteDatabase db = Database.getDB(context);

        ContentValues values = new ContentValues();
        values.put(DatabaseObjects.MessagesTable.COLUMN_READ, Constants.MESSAGE_STATUS_READ);

        db.update(DatabaseObjects.MessagesTable.TABLE_NAME,values, DatabaseObjects.MessagesTable.COLUMN_SENDER + " = ? and " + DatabaseObjects.MessagesTable.COLUMN_OWNER + " = ?", new String[]{username, Self.Contact.getUsername()});

        return null;
    }
}
