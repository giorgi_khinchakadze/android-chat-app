package com.example.giorgi.chatapp.AsyncTasks;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.example.giorgi.chatapp.Adapters.ChatRecyclerAdapter;
import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.Database.Database;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.Objects.Message;
import com.example.giorgi.chatapp.Database.SQLiteClasses.DatabaseObjects;
import com.example.giorgi.chatapp.Database.SQLiteClasses.DatabaseObjects.MessagesTable;
import com.example.giorgi.chatapp.Self;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by giorgi on 03.12.2016 16:29.
 */

public class MessagesLoader extends AsyncTask<Void,Message,Void> {

    private final Context context;
    private final ChatRecyclerAdapter adapter;
    private final Contact contact;

    public MessagesLoader(ChatRecyclerAdapter chatRecyclerAdapter, Contact contactId, Context context) {
        adapter = chatRecyclerAdapter;
        this.contact = contactId;
        this.context = context;
    }


    @Override
    protected Void doInBackground(Void... voids) {

        SQLiteDatabase db = Database.getDB(context);


        String query = String.format("select * from  %s m where ? in ( m.%s , m.%s ) and %s = ? order by m.%s desc",
                                MessagesTable.TABLE_NAME,
                                MessagesTable.COLUMN_SENDER,
                                MessagesTable.COLUMN_RECEIVER,
                                MessagesTable.COLUMN_OWNER,
                                MessagesTable.COLUMN_ID
        );

        Cursor c = db.rawQuery(query,new String[]{contact.getUsername(), Self.Contact.getUsername()});

        int messageCount = c.getCount();

        c.moveToFirst();
        while (!this.isCancelled() && !c.isAfterLast()){

            Contact sender;
            Contact receiver;
            Date date = null;
            String text = c.getString(3);
            int id = c.getInt(0);
            int status = c.getInt(5);

            try {
                date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse( c.getString(4));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            if (c.getString(1).equals(contact.getUsername())){
                sender = contact;
                receiver = Self.Contact;
            }else {
                receiver = contact;
                sender = Self.Contact;
            }

            Message m = new Message(id,sender,receiver,text,date,status);

            ContentValues values = new ContentValues();
            values.put(DatabaseObjects.MessagesTable.COLUMN_READ, Constants.MESSAGE_STATUS_READ);

            db.update(DatabaseObjects.MessagesTable.TABLE_NAME,values, DatabaseObjects.MessagesTable.COLUMN_ID + " = ? and " + DatabaseObjects.MessagesTable.COLUMN_OWNER + " = ?", new String[]{String.valueOf(id),Self.Contact.getUsername()});

            publishProgress(m);

            c.moveToNext();
        }
        c.close();


        if (messageCount <= 20) return null;

        String where = String.format("%s = ? and %s in (select %s from %s where %s = ? and ? in(%s,%s) order by %s desc limit -1 offset 20)",
                MessagesTable.COLUMN_OWNER,
                MessagesTable.COLUMN_ID,
                MessagesTable.COLUMN_ID,
                MessagesTable.TABLE_NAME,
                MessagesTable.COLUMN_OWNER,
                MessagesTable.COLUMN_RECEIVER,
                MessagesTable.COLUMN_SENDER,
                MessagesTable.COLUMN_ID
        );

        int k = db.delete(MessagesTable.TABLE_NAME,where,new String[]{Self.Contact.getUsername(),Self.Contact.getUsername(),contact.getUsername()});


        return null;
    }

    @Override
    protected void onProgressUpdate(Message... values) {
        adapter.addToTop(values[0]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        adapter.setLoading(false);
        super.onPostExecute(aVoid);
    }

    @Override
    protected void onPreExecute() {
        adapter.setLoading(true);
        super.onPreExecute();
    }

}
