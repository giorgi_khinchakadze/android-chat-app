package com.example.giorgi.chatapp.AsyncTasks;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.example.giorgi.chatapp.Adapters.RecentMessagesAdapter;
import com.example.giorgi.chatapp.Database.Database;
import com.example.giorgi.chatapp.Database.DatabaseFunctions;
import com.example.giorgi.chatapp.Database.Objects.Message;
import com.example.giorgi.chatapp.Database.SQLiteClasses.DatabaseObjects.ContactsTable;
import com.example.giorgi.chatapp.Database.SQLiteClasses.DatabaseObjects.MessagesTable;
import com.example.giorgi.chatapp.Self;

/**
 * Created by giorgi on 04.12.2016 15:19.
 */
public class RecentConversationLoader extends AsyncTask<Void,Message,Void> {
    private final RecentMessagesAdapter adapter;
    private final Context context;


    public RecentConversationLoader(RecentMessagesAdapter recentMessagesAdapter,Context context) {

        this.adapter = recentMessagesAdapter;
        this.context = context;
    }


    @Override
    protected Void doInBackground(Void... voids) {
        Database.LOCK.lock();
        Database.LOCK.unlock();

        SQLiteDatabase db = Database.getDB(context);

        String query = String.format("select c.* from %s c where %s = ?",ContactsTable.TABLE_NAME,ContactsTable.COLUMN_OWNER);
        Cursor c = db.rawQuery(query,new String[]{Self.Contact.getUsername()});

        while (!this.isCancelled() && c.moveToNext()){

            query = String.format("select * from %s m where %s = ? and ? in (m.%s ,m.%s ) order by %s desc limit 1",
                                    MessagesTable.TABLE_NAME,
                                    MessagesTable.COLUMN_OWNER,
                                    MessagesTable.COLUMN_RECEIVER,
                                    MessagesTable.COLUMN_SENDER,
                                    MessagesTable.COLUMN_ID
                                    );

            Cursor c1 = db.rawQuery(query,new String[]{Self.Contact.getUsername(),c.getString(1)});


            if (c1.moveToNext()){
                publishProgress(DatabaseFunctions.getMessage(c1,context));
            }
            c1.close();
        }
        c.close();

        return null;
    }

    @Override
    protected void onProgressUpdate(Message... values) {
        if (values[0] != null)
        adapter.addEntry(values[0]);
    }


}
