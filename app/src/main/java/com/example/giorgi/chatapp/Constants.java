package com.example.giorgi.chatapp;

/**
 * Created by giorgi on 05.01.2017 23:20.
 */

public class Constants {


    public static final String CHAT_SERVER = "http://wordgame.cloudapp.net:80";
//      public static final String CHAT_SERVER = "http://192.168.3.106:9999";

    public static final String REGISTER_SUCCESS = "rs";
    public static final String REGISTER_FAILURE = "rf";

    public static final String LOGIN_SUCCESS = "ls";
    public static final String LOGIN_FAILURE = "lf";

    public static final String SEARCH_SUCCESS = "ss";
    public static final String SEARCH_FAILURE = "sf";

    public static final String CONTACT_LIST_SUCCESS = "cls";
    public static final String CONTACT_LIST_FAILURE = "clf";



    public static final String TOKEN_LOGIN_SUCCESS = "tls";
    public static final String TOKEN_LOGIN_FAILURE = "tlf";

    public static final boolean SERVER_STATUS_CONNECTED = true;
    public static final boolean SERVER_STATUS_UNRESPONSIVE = false;

    public static final int CONTACT_STATUS_ONLINE = 1;
    public static final int CONTACT_STATUS_OFFLINE = 0;

    public static final String TOKEN_INVALID = "ti";

    public static final String CONTACT_ADDED = "contact-added";
    public static final String CONTACT_DELETED = "contact-deleted";
    public static final String CONTACT_STATUS_CHANGED = "contact-status";


    public static final String MESSAGE_RECEIVED = "message-received";
    public static final String MESSAGE_SENT = "message-sent";
    public static final String MESSAGE_CONSUMED = "message-consumed";


    public static final int MESSAGE_STATUS_READ = 1;
    public static final int MESSAGE_STATUS_NOT_READ = 0;


}
