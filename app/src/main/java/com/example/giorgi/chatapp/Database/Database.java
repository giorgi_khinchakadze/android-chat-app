package com.example.giorgi.chatapp.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.giorgi.chatapp.Database.SQLiteClasses.DbHelper;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by giorgi on 30.11.2016 20:15 11:53 11:53 11:53.
 */

public class Database {

    private static volatile SQLiteDatabase db9 = null;
    private static final Object lock = new Object();
    public static final Lock  LOCK = new ReentrantLock();

    public static SQLiteDatabase getDB(Context context){
        SQLiteDatabase db = db9;
        if (db == null || !db.isOpen()){
            synchronized (lock){
                db = db9;
                if (db == null || !db.isOpen()){
                    db = db9 = new DbHelper(context).getWritableDatabase();
                }
            }
        }

        return db;
    }



}
