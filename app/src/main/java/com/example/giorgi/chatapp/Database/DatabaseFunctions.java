package com.example.giorgi.chatapp.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.Objects.Message;
import com.example.giorgi.chatapp.Database.SQLiteClasses.DatabaseObjects;
import com.example.giorgi.chatapp.Events.ContactEvents;
import com.example.giorgi.chatapp.Self;
import com.example.giorgi.chatapp.SocketIO.SocketIO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by giorgi on 04.12.2016 19:44.
 */

public class DatabaseFunctions {

    public static Contact getContact(Cursor c){
        return new Contact(c.getString(1),c.getString(2),c.getString(3),c.getString(4), SocketIO.getStatus() == Constants.SERVER_STATUS_UNRESPONSIVE ? Constants.CONTACT_STATUS_OFFLINE:c.getInt(5));
    }

    public static Contact getContact(String username, Context context){
        if (username.equals(Self.Contact.getUsername())) return Self.Contact;

        Contact contact;

        String query = String.format("select * from %s where %s = ? and %s = ?", DatabaseObjects.ContactsTable.TABLE_NAME, DatabaseObjects.ContactsTable.COLUMN_NAME, DatabaseObjects.ContactsTable.COLUMN_OWNER);
        Cursor c = Database.getDB(context).rawQuery(query,new String[]{username,Self.Contact.getUsername()});

        c.moveToFirst();
        contact = getContact(c);
        c.close();

        return  contact;
    }

    public static Message getMessage(Cursor cursor,Context context) {

        try {

            Contact sender = DatabaseFunctions.getContact(cursor.getString(1), context);
            Contact receiver = DatabaseFunctions.getContact(cursor.getString(2), context);
            Date date = null;
            try {
                date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(cursor.getString(4));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return new Message(cursor.getInt(0), sender, receiver, cursor.getString(3), date, cursor.getInt(5));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void addContact(Contact c,Context context) {

        ContentValues values = new ContentValues();

        values.put(DatabaseObjects.ContactsTable.COLUMN_IMG,c.getImg());
        values.put(DatabaseObjects.ContactsTable.COLUMN_FIRSTNAME,c.getFirstname());
        values.put(DatabaseObjects.ContactsTable.COLUMN_LASTNAME,c.getLastname());
        values.put(DatabaseObjects.ContactsTable.COLUMN_NAME,c.getUsername());
        values.put(DatabaseObjects.ContactsTable.COLUMN_STATUS,c.getStatus());
        values.put(DatabaseObjects.ContactsTable.COLUMN_OWNER,Self.Contact.getUsername());

        Database.getDB(context).insert(DatabaseObjects.ContactsTable.TABLE_NAME,null,values);
    }

    public static void removeContact(String username, Context context) {
        Database.getDB(context).delete(DatabaseObjects.ContactsTable.TABLE_NAME, DatabaseObjects.ContactsTable.COLUMN_NAME + " = ? and " + DatabaseObjects.ContactsTable.COLUMN_OWNER + " = ?", new String[]{username,Self.Contact.getUsername()});

        Database.getDB(context).delete(DatabaseObjects.MessagesTable.TABLE_NAME,
                                       "? in ( " + DatabaseObjects.MessagesTable.COLUMN_SENDER + " , " + DatabaseObjects.MessagesTable.COLUMN_RECEIVER + " ) and " + DatabaseObjects.MessagesTable.COLUMN_OWNER + " = ?",
                                       new String[]{username,Self.Contact.getUsername()});
    }
    public static void updateContact(Contact c,Context context){

        ContentValues values = new ContentValues();

        values.put(DatabaseObjects.ContactsTable.COLUMN_IMG,c.getImg());
        values.put(DatabaseObjects.ContactsTable.COLUMN_FIRSTNAME,c.getFirstname());
        values.put(DatabaseObjects.ContactsTable.COLUMN_LASTNAME,c.getLastname());
        values.put(DatabaseObjects.ContactsTable.COLUMN_NAME,c.getUsername());
        values.put(DatabaseObjects.ContactsTable.COLUMN_STATUS,c.getStatus());
        values.put(DatabaseObjects.ContactsTable.COLUMN_OWNER,Self.Contact.getUsername());

        Database.getDB(context).update(DatabaseObjects.ContactsTable.TABLE_NAME,values, DatabaseObjects.ContactsTable.COLUMN_NAME + " = ? and " + DatabaseObjects.ContactsTable.COLUMN_OWNER + " = ?", new String[]{c.getUsername(),Self.Contact.getUsername()});
    }

    public static void updateContactsList(List<Contact> contacts, Context context) {
        Cursor c = Database.getDB(context).query(DatabaseObjects.ContactsTable.TABLE_NAME,null, DatabaseObjects.ContactsTable.COLUMN_OWNER + " = ?", new String[]{Self.Contact.getUsername()},null,null,null);

        outer:while (c.moveToNext()){
            String username = c.getString(1);
            for (Contact contact : contacts) {
                if (contact.getUsername().equals(username)) {
                    updateContact(contact, context);
                    contacts.remove(contact);
                    continue outer;
                }
            }

            ContactEvents.ContactDeleted(username);
        }
        c.close();

        for (Contact contact : contacts) {
            ContactEvents.ContactAdded(contact);
        }


    }


    public static void saveMessage(Message m,Context context){

        ContentValues values = new ContentValues();
        values.putNull(DatabaseObjects.MessagesTable.COLUMN_ID);
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(m.getSent());

        values.put(DatabaseObjects.MessagesTable.COLUMN_ID,m.getId());
        values.put(DatabaseObjects.MessagesTable.COLUMN_DATE,date);
        values.put(DatabaseObjects.MessagesTable.COLUMN_READ,m.getStatus());
        values.put(DatabaseObjects.MessagesTable.COLUMN_MESSAGE,m.getText());
        values.put(DatabaseObjects.MessagesTable.COLUMN_SENDER,m.getSender().getUsername());
        values.put(DatabaseObjects.MessagesTable.COLUMN_RECEIVER,m.getReceiver().getUsername());
        values.put(DatabaseObjects.MessagesTable.COLUMN_OWNER,Self.Contact.getUsername());
        m.setId((int) Database.getDB(context).insert(DatabaseObjects.MessagesTable.TABLE_NAME,null,values));

    }

    public static int GetLatestMessageId(Context context){
        int maxid = -1;
        String query = String.format("select max(%s) from %s where %s = ?", DatabaseObjects.MessagesTable.COLUMN_ID, DatabaseObjects.MessagesTable.TABLE_NAME, DatabaseObjects.MessagesTable.COLUMN_OWNER);
        Cursor c = Database.getDB(context).rawQuery(query, new String[]{Self.Contact.getUsername()});

        if (c.moveToFirst()) maxid = c.getInt(0);

        c.close();
        return  maxid;
    }


    public static void updateContactStatus(String username, int status,Context context) {


        ContentValues values = new ContentValues();
        values.put(DatabaseObjects.ContactsTable.COLUMN_STATUS,status);

        if (username == null){
            Database.getDB(context).update(DatabaseObjects.ContactsTable.TABLE_NAME,values, null,null);
        }else {
            Database.getDB(context).update(DatabaseObjects.ContactsTable.TABLE_NAME,values, DatabaseObjects.ContactsTable.COLUMN_NAME + " = ?", new String[]{username});
        }

    }
}
