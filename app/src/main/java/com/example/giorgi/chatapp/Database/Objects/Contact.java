package com.example.giorgi.chatapp.Database.Objects;

import android.content.SharedPreferences;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.example.giorgi.chatapp.BR;
import com.example.giorgi.chatapp.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;


public class Contact extends BaseObservable implements Parcelable {


    public Contact(){
        this.username = "";
        this.img = "";
        this.firstname = "";
        this.lastname = "";
    }
    public Contact( String username, String img, String firstname, String lastname, int status) {
        this.username = username;
        this.img = img;
        this.firstname = firstname;
        this.lastname = lastname;
        this.status = status;
    }



    @Bindable
    private String firstname;
    @Bindable
    private String lastname;
    @Bindable
    private String username;
    @Bindable
    private String img;
    @Bindable
    private int status = 0;


    private Contact(Parcel in) {
        username = in.readString();
        img = in.readString();
        firstname = in.readString();
        lastname = in.readString();
        status = in.readInt();
    }

    public Contact(JSONObject obj) throws JSONException {

        username = obj.getString("username");
        img = obj.getString("image");
        firstname = obj.getString("firstname");
        lastname = obj.getString("lastname");

    }

    public void setUsername(String username){
        this.username = username;
        this.notifyPropertyChanged(BR.username);
    }
    public void setImg(String img){
        this.img = img;
        this.notifyPropertyChanged(BR.img);
    }
    public void setStatus(int status){
        this.status = status;
        this.notifyPropertyChanged(BR.status);
    }


    public String getUsername() {
        return username;
    }

    public String getImg() {
        return img;
    }

    public int getStatus() {
        return status;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }



    @BindingAdapter({"android:src"})
    public static void loadImage(ImageView view, String url) {
        Picasso.with(view.getContext()).load(url).fit().placeholder(R.drawable.ic_account_circle_black_48dp).into(view);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(img);
        parcel.writeString(firstname);
        parcel.writeString(lastname);
        parcel.writeInt(status);
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };


    public void update(String username, String image, String firstname, String lastname) {
        this.username = username;
        this.img = image;
        this.firstname = firstname;
        this.lastname = lastname;
    }


    public void update(SharedPreferences preferences) {
        username = preferences.getString("username","");
        img = preferences.getString("image","");
        firstname = preferences.getString("firstname","");
        lastname = preferences.getString("lastname","");

    }
}

