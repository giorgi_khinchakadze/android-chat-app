package com.example.giorgi.chatapp.Database.Objects;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by giorgi on 30.11.2016 22:40.
 */

public class Message {

    private int id;
    private String text;
    private Date sent;
    private int status;


    private Contact sender;
    private Contact receiver;


    public Message( int id,Contact sender, Contact receiver, String text, Date sent, int status) {
        this.id = id;
        this.sender = sender;
        this.text = text;
        this.sent = sent;
        this.status = status;
        this.receiver = receiver;
    }

    public Message(JSONObject jsonObject) throws JSONException, ParseException {

        this.sender = new Contact(jsonObject.getJSONObject("sender"));
        this.receiver = new Contact(jsonObject.getJSONObject("receiver"));
        this.id = jsonObject.getInt("id");
        this.status = jsonObject.getInt("status");
        this.text = jsonObject.getString("text");
        this.sent = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(jsonObject.getString("date"));

    }


    public int getId() {
        return id;
    }

    public Contact getSender() {
        return sender;
    }
    public Contact getReceiver() {
        return receiver;
    }

    public String getText() {
        return text;
    }

    public Date getSent() {
        return sent;
    }

    public int getStatus() {
        return status;
    }

    public void setId(int id) {
        this.id = id;
    }
}
