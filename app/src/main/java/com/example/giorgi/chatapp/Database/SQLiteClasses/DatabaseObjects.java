package com.example.giorgi.chatapp.Database.SQLiteClasses;

/**
 * Created by giorgi on 30.11.2016 22:06.
 */

public final class DatabaseObjects {

    private static final String TYPE_TEXT = " TEXT";
    private static final String TYPE_INTEGER = " INTEGER";
    private static final String SEPARATOR = ",";
    private static final String PRIMARY = " PRIMARY KEY";
    private static final String TYPE_DATETIME = " DATETIME";
    private static final String AUTOINCREMENT = " AUTOINCREMENT";


    public static abstract class ContactsTable {
        public static final String TABLE_NAME = "contacts";
        public static final String COLUMN_ID = "contact_id";
        public static final String COLUMN_NAME = "contact_name";
        public static final String COLUMN_FIRSTNAME = "contact_firstname";
        public static final String COLUMN_LASTNAME = "contact_lastname";
        public static final String COLUMN_IMG = "contact_image";
        public static final String COLUMN_STATUS = "contact_status";
        public static final String COLUMN_OWNER = "contact_owner";

        static final String CREATE_SQL = "CREATE TABLE " + TABLE_NAME + "( " +
                COLUMN_ID + TYPE_INTEGER + PRIMARY + SEPARATOR +
                COLUMN_NAME + TYPE_TEXT + SEPARATOR +
                COLUMN_IMG + TYPE_TEXT + SEPARATOR +
                COLUMN_FIRSTNAME + TYPE_TEXT + SEPARATOR +
                COLUMN_LASTNAME + TYPE_TEXT +  SEPARATOR +
                COLUMN_STATUS + TYPE_INTEGER + SEPARATOR +
                COLUMN_OWNER + TYPE_TEXT +
                " )";
        static final String CREATE_INDEXES = "CREATE UNIQUE INDEX IF NOT EXISTS uk_contacts ON " + TABLE_NAME + " ( " + COLUMN_NAME + " , " + COLUMN_OWNER + " ) ";

        static final String DELETE_SQL = "DROP TABLE IF EXISTS " + TABLE_NAME;

    }

    public static abstract class MessagesTable {
        public static final String TABLE_NAME = "messages";
        public static final String COLUMN_ID = "message_id";
        public static final String COLUMN_SENDER = "message_sender";
        public static final String COLUMN_RECEIVER = "message_receiver";
        public static final String COLUMN_MESSAGE = "message_text";
        public static final String COLUMN_DATE = "message_sent_date";
        public static final String COLUMN_READ = "message_read_status";
        public static final String COLUMN_OWNER = "message_owner";



        static final String CREATE_SQL = "CREATE TABLE " + TABLE_NAME + "( " +
                COLUMN_ID + TYPE_INTEGER +  SEPARATOR +
                COLUMN_SENDER + TYPE_TEXT + SEPARATOR +
                COLUMN_RECEIVER + TYPE_TEXT + SEPARATOR +
                COLUMN_MESSAGE + TYPE_TEXT + SEPARATOR +
                COLUMN_DATE + TYPE_DATETIME + SEPARATOR +
                COLUMN_READ + TYPE_INTEGER + SEPARATOR +
                COLUMN_OWNER + TYPE_TEXT +
                " )";

        static final String CREATE_INDEXES = "CREATE UNIQUE INDEX IF NOT EXISTS uk_messages ON " + TABLE_NAME + " ( " + COLUMN_ID + " , " + COLUMN_OWNER + " ) ";


        static final String DELETE_SQL = "DROP TABLE IF EXISTS " + TABLE_NAME;


    }

}
