package com.example.giorgi.chatapp.Database.SQLiteClasses;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by giorgi on 30.11.2016 22:19.
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final String NAME = "db9";
    private static final int VERSION = 15;

    public DbHelper(Context context) {
        super(context, NAME,null,VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("ChatApp", "onCreate: Database");
        sqLiteDatabase.execSQL(DatabaseObjects.ContactsTable.CREATE_SQL);
        sqLiteDatabase.execSQL(DatabaseObjects.MessagesTable.CREATE_SQL);
        sqLiteDatabase.execSQL(DatabaseObjects.ContactsTable.CREATE_INDEXES);
        sqLiteDatabase.execSQL(DatabaseObjects.MessagesTable.CREATE_INDEXES);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DatabaseObjects.MessagesTable.DELETE_SQL);
        sqLiteDatabase.execSQL(DatabaseObjects.ContactsTable.DELETE_SQL);
        onCreate(sqLiteDatabase);
    }
}
