package com.example.giorgi.chatapp.Events;

import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Events.Interfaces.ContactEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giorgi on 07.01.2017 22:00.
 */

public class ContactEvents {

    private static final List<ContactEventListener> listeners = new ArrayList<>();

    public static void RegisterListener(ContactEventListener listener){
        listeners.add(listener);
    }
    public static void UnregisterListener(ContactEventListener listener){
        listeners.remove(listener);
    }

    public static void ContactAdded(Contact c){

        for (ContactEventListener listener : listeners) {
            listener.contactAdded(c);
        }
    }

    public static void ContactDeleted(String username){
        for (ContactEventListener listener : listeners) {
            listener.contactDeleted(username);
        }
    }

    public static void ContactStatusChanged(String username,int status){
        for (ContactEventListener listener : listeners) {
            listener.contactStatusChanged(username,status);
        }
    }

    public static void Reset() {
        listeners.clear();
    }
}
