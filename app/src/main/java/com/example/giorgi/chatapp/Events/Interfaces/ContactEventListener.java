package com.example.giorgi.chatapp.Events.Interfaces;

import com.example.giorgi.chatapp.Database.Objects.Contact;

/**
 * Created by giorgi on 07.01.2017 22:01.
 */

public interface ContactEventListener {

    void contactDeleted(String username);
    void contactAdded(Contact username);
    void contactStatusChanged(String username, int status);
}
