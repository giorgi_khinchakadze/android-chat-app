package com.example.giorgi.chatapp.Events.Interfaces;

import com.example.giorgi.chatapp.Database.Objects.Message;

/**
 * Created by giorgi on 03.12.2016 18:48.
 */
public interface MessageEventListener {

    boolean  onMessageReceived(Message message);
    void     onMessagesConsumed(String username);
    void     onMessageSent(Message message);

}
