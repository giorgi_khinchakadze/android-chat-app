package com.example.giorgi.chatapp.Events;

import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.Objects.Message;
import com.example.giorgi.chatapp.Events.Interfaces.MessageEventListener;
import com.example.giorgi.chatapp.Self;
import com.example.giorgi.chatapp.SocketIO.SocketIO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giorgi on 03.12.2016 18:47.
 */

public class MessageEvents {


    private static final List<MessageEventListener> listeners = new ArrayList<>();

    public static void registerListener(MessageEventListener l){
        listeners.add(l);
    }

    public static void unregisterListener(MessageEventListener l){
        listeners.remove(l);
    }

    public static void MessageReceived(Message m){

        boolean consumed = false;
        for (MessageEventListener listener : listeners) {
            consumed = consumed|listener.onMessageReceived(m);
        }

        if (!consumed) return;


        if (m.getStatus() == Constants.MESSAGE_STATUS_NOT_READ){
            SocketIO.ConsumeMessages(m.getSender(),null);
        }

    }

    public static void MessageConsumed(String value) {
        for (MessageEventListener listener : listeners) {
            listener.onMessagesConsumed(value);
        }
    }



    public static void MessageSent(Message m) {
        for (MessageEventListener listener : listeners) {
            listener.onMessageSent(m);
        }
    }

    public static void SendMessage(Contact contact, String s) {
        Message m = new Message(-1, Self.Contact,contact,s,null, Constants.MESSAGE_STATUS_READ);
        SocketIO.SendMessage(m,null);
    }


    public static void Reset() {
        listeners.clear();
    }
}
