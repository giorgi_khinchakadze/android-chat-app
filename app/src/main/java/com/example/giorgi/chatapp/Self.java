package com.example.giorgi.chatapp;

import android.content.Context;

import com.example.giorgi.chatapp.Database.Objects.Contact;

/**
 * Created by giorgi on 03.12.2016 19:13.
 */

public class Self {

    public static final Contact Contact = new Contact();
    public static String Token = "";

    public static void setNewInfo(Context context, String info){
        Utilities.UpdateSelfInfo(info);
        context.getSharedPreferences("main", Context.MODE_PRIVATE)
                .edit()
                .putString("token", Self.Token)
                .putString("username",Contact.getUsername())
                .putString("firtname",Contact.getFirstname())
                .putString("lastname",Contact.getLastname())
                .putString("image",Contact.getImg())
                .apply();
    }

    public static String getOldToken(Context context) {
        Token = context.getSharedPreferences("main",Context.MODE_PRIVATE).getString("token","");
        Contact.update( context.getSharedPreferences("main",Context.MODE_PRIVATE));
        return Token;
    }
}

