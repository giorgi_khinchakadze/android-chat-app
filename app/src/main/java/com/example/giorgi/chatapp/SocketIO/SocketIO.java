package com.example.giorgi.chatapp.SocketIO;

import android.os.Handler;
import android.os.Looper;

import com.example.giorgi.chatapp.Constants;
import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.Objects.Message;
import com.example.giorgi.chatapp.Events.ContactEvents;
import com.example.giorgi.chatapp.Events.MessageEvents;
import com.example.giorgi.chatapp.Self;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


/**
 * Created by giorgi on 05.01.2017 20:06.
 */




public class SocketIO {
    private static Socket socket ;
    static {
        try {
            socket = IO.socket(Constants.CHAT_SERVER);
            socket.connect();

            Emitter.Listener listener = new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    for (SocketStatusChangeListener statusChangeListener : statusChangeListeners) {
                        statusChangeListener.statusChanged(socket.connected());
                    }
                }
            };
            socket.on(Socket.EVENT_RECONNECTING, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    socket.io().reconnectionDelay(socket.io().reconnectionDelay() / 2);
                }
            });

            socket.on(Socket.EVENT_CONNECT,listener);
            socket.on(Socket.EVENT_DISCONNECT, listener);
            socket.on(Constants.CONTACT_ADDED, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {
                        final Contact c = new Contact(new JSONObject((String) args[0]));
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                ContactEvents.ContactAdded(c);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            socket.on(Constants.CONTACT_STATUS_CHANGED, new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            ContactEvents.ContactStatusChanged(args[0].toString(), Integer.parseInt(args[1].toString()));
                        }
                    });
                }
            });

            socket.on(Constants.MESSAGE_RECEIVED, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {
                        final Message m = new Message(new JSONObject((String) args[0]));

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                MessageEvents.MessageReceived(m);
                            }
                        });

                    } catch (JSONException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            });


            socket.on(Constants.MESSAGE_SENT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {

                        final Message m = new Message(new JSONObject((String) args[0]));

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                MessageEvents.MessageSent(m);
                            }
                        });

                    } catch (JSONException | ParseException e) {
                        e.printStackTrace();
                    }

                }
            });

            socket.on(Constants.MESSAGE_CONSUMED, new Emitter.Listener() {
                @Override
                public void call(final Object... args) {

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            MessageEvents.MessageConsumed(args[0].toString());
                        }
                    });

                }
            });






        } catch (URISyntaxException ignored) {}
    }

    private static final Set<SocketStatusChangeListener> statusChangeListeners = new HashSet<>();


    public static void registerStatusChangeListener(SocketStatusChangeListener listener){
        statusChangeListeners.add(listener);
    }
    public static void unregisterStatusChangeListener(SocketStatusChangeListener listener){
        statusChangeListeners.remove(listener);
    }



    public static boolean getStatus(){
        return socket.connected();
    }

    public static void RegisterUser(String username,String firstname,String lastname,String password,Ack ack){

        JSONObject obj = new JSONObject();
        try {
            obj.put("mail", username);
            obj.put("firstname", firstname);
            obj.put("lastname", lastname);
            obj.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("register", obj.toString(), ack);

    }

    public static void LoginUser(String username,String password,Ack ack){

        JSONObject obj = new JSONObject();
        try {
            obj.put("mail", username);
            obj.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("login", obj.toString(), ack);

    }

    public static void TokenLogin(String token,Ack ack){
        socket.emit("token-login",token,ack);
    }

    public static void GetContactListFor(String username,Ack ack) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("token", Self.Token);
            obj.put("mail", username);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("search-contacts",obj.toString(),ack);

    }

    public static void AddContact(Contact contact,Ack ack) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("token", Self.Token);
            obj.put("mail", contact.getUsername());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("add-contact",obj.toString(),ack);

    }

    public static void GetCurrentContactList(Ack ack) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("token", Self.Token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("get-contacts",obj.toString(),ack);

    }

    public static void SendMessage(Message m,Ack ack) {
        JSONObject obj = new JSONObject();
        try {

            obj.put("token", Self.Token);
            obj.put("to",m.getReceiver().getUsername());
            obj.put("message",m.getText());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        socket.emit("message-send",obj.toString(),ack);

    }

    public static void ConsumeMessages(Contact c, Ack ack) {
        JSONObject obj = new JSONObject();
        try {

            obj.put("token", Self.Token);
            obj.put("username",c.getUsername());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        socket.emit("message-consume",obj.toString(),ack);

    }

    public static void RequestMessagesAfter(int last) {

        JSONObject obj = new JSONObject();
        try {

            obj.put("token", Self.Token);
            obj.put("last",last);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("message-request",obj.toString());

    }

    public static void Reset() {
        statusChangeListeners.clear();
        SocketIO.socket.disconnect().connect();
    }

    public static void GetMessagesBefore(int firstMessageId,String username,Ack ack) {

        JSONObject obj = new JSONObject();
        try {

            obj.put("token", Self.Token);
            obj.put("first",firstMessageId);
            obj.put("username",username);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("message-request-before",obj.toString(),ack);

    }
}
