package com.example.giorgi.chatapp.SocketIO;

/**
 * Created by giorgi on 06.01.2017 23:39.
 */

public interface SocketStatusChangeListener {

    void statusChanged(boolean status);

}
