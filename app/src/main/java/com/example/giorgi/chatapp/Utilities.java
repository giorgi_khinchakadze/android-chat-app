package com.example.giorgi.chatapp;

import com.example.giorgi.chatapp.Database.Objects.Contact;
import com.example.giorgi.chatapp.Database.Objects.Message;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by giorgi on 07.01.2017 15:14.
 */
public class Utilities {
    public static List<Contact> JsonToContactList(String s) {
        List<Contact> list = new LinkedList<>();

        try {
            JSONArray arr = new JSONArray(s);

            for (int i = 0; i < arr.length(); i++) {

                JSONObject tmp = arr.getJSONObject(i);
                String username = tmp.getString("username");
                String firstname = tmp.getString("firstname");
                String lastname = tmp.getString("lastname");
                String image = tmp.getString("image");
                int status = tmp.getInt("status");

                Contact tmpc = new Contact(username,image,firstname,lastname,status);
                list.add(tmpc);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return list;
    }

    public static void UpdateSelfInfo(String info) {
        try {
            JSONObject tmp = new JSONObject(info);
            String username = tmp.getString("username");
            String firstname = tmp.getString("firstname");
            String lastname = tmp.getString("lastname");
            String image = tmp.getString("image");

            Self.Contact.update(username,image,firstname,lastname);

            Self.Token = tmp.getString("token");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    public static List<Message> JsonToMessageList(String arg) {
        List<Message> messages = new LinkedList<>();

        try {
            JSONArray arr = new JSONArray(arg);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject tmp = arr.getJSONObject(i);
                Message message = new Message(tmp);
                messages.add(message);
            }

        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }


        return messages;
    }
}
